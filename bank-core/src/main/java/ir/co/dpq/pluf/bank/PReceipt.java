package ir.co.dpq.pluf.bank;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "Receipts")
@Table(name = "Receipts")
public class PReceipt implements Serializable {

	private static final long serialVersionUID = -8670521185187585208L;

	@Id
	private long id;

	@Column(name = "secure_id")
	private String secureId;

	private long amount;

	private String title;

	private String description;

	private String email;

	private String phone;

	@Column(name = "callBackURL")
	private String callBackUrl;

	private String payRef;

	@Column(name = "callURL")
	private String callUrl;

	private String payMeta;

	private long backend;

	@Column(name = "owner_id")
	private long ownerId;

	@Column(name = "owner_class")
	private String ownerClass;

	@Column(name = "creation_dtime")
	@Temporal(TemporalType.DATE)
	private Date creation;

	@Column(name = "modif_dtime")
	@Temporal(TemporalType.DATE)
	private Date modification;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSecureId() {
		return secureId;
	}

	public void setSecureId(String secureId) {
		this.secureId = secureId;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getPayRef() {
		return payRef;
	}

	public void setPayRef(String payRef) {
		this.payRef = payRef;
	}

	public String getCallUrl() {
		return callUrl;
	}

	public void setCallUrl(String callUrl) {
		this.callUrl = callUrl;
	}

	public String getPayMeta() {
		return payMeta;
	}

	public void setPayMeta(String payMeta) {
		this.payMeta = payMeta;
	}

	public long getBackend() {
		return backend;
	}

	public void setBackend(long backend) {
		this.backend = backend;
	}

	public long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}

	public String getOwnerClass() {
		return ownerClass;
	}

	public void setOwnerClass(String ownerClass) {
		this.ownerClass = ownerClass;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Date getModification() {
		return modification;
	}

	public void setModification(Date modification) {
		this.modification = modification;
	}

}
